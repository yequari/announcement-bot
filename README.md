# announcement-bot

## Features
- Watches specific channels for new messages and new pins
- Writes out messages in various formats (HTML, XML, etc)

## Channel Registration
Once the bot is added to a server, register channels to be watched with /register command

## Export
- Preserves Discord formatting

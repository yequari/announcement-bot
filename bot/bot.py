import os
import discord
import database


def read_env(file):
    for line in file:
        splitline = line.split('=', maxsplit=1)
        if len(splitline) == 2:
            os.environ[splitline[0]] = splitline[1]


class AnnouncementWatcherClient(discord.Client):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.db = database.DatabaseManager('announcement.db')

    async def on_guild_join(self, guild):
        self.db.create_guild()

    # TODO: on_guild_leave() delete all data?

    async def on_message(self, message: discord.Message):
        if message.author == self.user:
            return

        if message.content.startswith('$hello'):
            await message.channel.send('Hello!')

    async def on_guild_channel_pins_update(self, channel: discord.GuildChannel, last_pin):
        pass

    def register_channel(self):
        pass

    def unregister_channel(self):
        pass


if __name__ == "__main__":
    with open('.env') as f:
        read_env(f)
    discord_token = os.environ.get('DISCORD_TOKEN')
    if discord_token is None:
        print('Could not read token')
        exit(1)
    intents = discord.Intents.default()
    intents.message_content = True
    client = AnnouncementWatcherClient(intents=intents)
    client.run(discord_token)

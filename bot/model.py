from dataclasses import dataclass
from datetime import datetime


class WatchObject:
    table = ''

    @classmethod
    def fields(cls):
        '''Returns the field names in the database'''
        raise NotImplementedError


@dataclass
class WatchGuild(WatchObject):
    '''WatchGuild represents a Discord guild as stored in the database.

    Attributes:
        id: Guild ID as given by Discord
        name: Name of the guild at the time of adding the bot
    '''
    id: int
    name: str
    join_date: datetime
    table: str = 'guilds'

    @classmethod
    def fields(cls):
        return ['guild_id', 'name', 'join_date']


@dataclass
class WatchChannel(WatchObject):
    '''WatchChannel represents a Discord channel being watched for new messages

    Attributes:
        id: Channel ID as given by Discord
        name: Name of the channel at the time of registration
        register_date: Datetime object when the channel was registered
        guild: WatchGuild object of which the channel belongs to
    '''
    id: int
    name: str
    register_date: datetime
    guild: WatchGuild
    table = 'channels'

    @classmethod
    def fields(cls):
        return ['channel_id', 'name', 'register_date', 'guild_id']


@dataclass
class WatchUser(WatchObject):
    '''WatchUser represents a Discord user who has sent a message in a watched channel.
    Attributes:
        id: User ID as given by Discord
        name: Nickname in the server at the time of latest message recorded
    '''
    id: int
    name: str
    table = 'users'

    @classmethod
    def fields(cls):
        return ['user_id', 'name']


@dataclass
class WatchMessage(WatchObject):
    '''WatchMessage represents a Discord message sent in a watched channel.

    Attributes:
        id: Message ID as given by Discord.
        contents: Raw contents of the message.
        published_date: Datetime object representing when the message was sent.
        author: WatchUser who sent the message.
        channel: WatchChannel in which the message was sent.
        guild: WatchGuild in which the message was sent.
    '''
    id: int
    contents: str
    published_date: datetime
    author: WatchUser
    channel: WatchChannel
    guild: WatchGuild
    table = 'messages'

    @classmethod
    def fields(cls):
        return ['message_id', 'contents', 'published_date',
                'user_id', 'channel_id', 'guild_id']

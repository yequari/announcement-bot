import sqlite3
from model import WatchGuild, WatchChannel, WatchUser, WatchMessage


class DatabaseManager:
    '''
    DatabaseManager manages all database operations

    Contains a single databse connection object.

    Attributes:
        name: name of the database file.
        conn: sqlite3 connection object to the database file.
    '''

    def __init__(self, name: str):
        '''
        Create a DatabaseManager

        Initializes a DatabaseManager and a sqlite3 database connection.
        If the database file does not exist, create one and the required tables
        name: name of the sqlite file
        '''
        self.name = name
        self.conn = sqlite3.connect(self.name)
        self._setup_database()

    def _setup_database(self):
        script = '''BEGIN;
                CREATE TABLE IF NOT EXISTS guilds(
                    guild_id INTEGER PRIMARY KEY,
                    name TEXT,
                    join_date TEXT);
                CREATE TABLE IF NOT EXISTS channels(
                    channel_id INTEGER PRIMARY KEY,
                    name TEXT,
                    register_date TEXT,
                    guild_id INTEGER,
                    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id));
                CREATE TABLE IF NOT EXISTS users(
                    user_id INTEGER PRIMARY KEY,
                    name TEXT);
                CREATE TABLE IF NOT EXISTS messages(
                    message_id INTEGER PRIMARY KEY,
                    contents TEXT,
                    published_date TEXT,
                    user_id INTEGER,
                    channel_id INTEGER,
                    guild_id INTEGER,
                    FOREIGN KEY (user_id) REFERENCES users(user_id),
                    FOREIGN KEY (channel_id) REFERENCES channels(channel_id),
                    FOREIGN KEY (guild_id) REFERENCES guilds(guild_id));
                COMMIT;'''

        with self.conn:
            self.conn.executescript(script)

    def _build_select_query(self, cls, **kwargs) -> (str, tuple):
        wildcards = ['?=?' * len(kwargs)]
        query = f'''SELECT {','.join(cls.fields())} FROM {cls.table}
                WHERE {','.join(wildcards)}'''
        constraints = []
        for k, v in kwargs.items():
            constraints.append(k)
            constraints.append(v)
        return query, tuple(constraints)

    def create_guild(self, guild: WatchGuild):
        '''Insert a new guild into the database'''
        query = '''INSERT INTO guilds VALUES(?, ?, datetime('now'));'''
        with self.conn:
            self.conn.execute(query, (guild.id, guild.name))

    def get_guild(self, **kwargs) -> list[WatchGuild]:
        '''Access guilds stored in the Database.

        Query will be filtered by the passed parameters,
        returning records whose field values match those passed in.
        '''
        query = '''SELECT guild_id, name, join_date FROM guilds;'''
        with self.conn:
            cur = self.conn.execute(query)
            return cur.fetchall()

    def create_channel(self, channel: WatchChannel):
        '''Insert a new channel into the database'''
        query = '''INSERT INTO channels VALUES(?, ?, ?, ?);'''
        with self.conn:
            self.conn.execute(query, (channel.id, channel.name,
                                      channel.register_date, channel.guild.id))

    def get_channel(self, **kwargs):
        query = '''SELECT channel_id, name,
                register_date, guild_id FROM channels;'''
        with self.conn:
            curs = self.conn.execute(query)
            return curs.fetchall()

    def create_user(self, user: WatchUser):
        pass

    def get_user(self, **kwargs):
        pass

    def create_message(self, message: WatchMessage):
        pass

    def get_message(self, **kwargs):
        pass
